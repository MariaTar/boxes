const webpack = require('webpack');
const path = require('path');
const htmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

module.exports = {
    context: path.join(__dirname, 'src'),

    entry: './js/index.js',

    output: {
        filename: 'bundle.js',
        path: path.resolve(__dirname, 'dist')
    },

    devtool: 'source-map',

    devServer: {
        port: 4200
    },

    module: {
        rules: [
        {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: 'babel-loader'
         },{
            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: [
                {
                    loader: 'css-loader',
                    options: {importLoaders: 10}
                // },{
                //     loader: 'postcss-loader',
                //     options: {sourceMap: true}
                },
                'resolve-url-loader',
                'sass-loader?includePaths[]='+path.resolve(__dirname, './src'),
                ]
            })
        },{
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: [
                {
                    loader: 'css-loader',
                    options: {importLoaders: 1}
                },
                'resolve-url-loader',
                ]
            })
        },{
            test: /\.(png|jpg|gif|svg)$/i,
            loader: 'resolve-url-loader',
            options: {
                name: '[path][name].[ext]',
                publicPath: './image'
            }
        }]
    },

    plugins: [
        new htmlWebpackPlugin({
            title: 'boxes',
            template: '../index.html'
        }),
        new ExtractTextPlugin('main.css')
]};